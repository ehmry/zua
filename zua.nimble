# Package

version       = "0.1.0"
author        = "Emery Hemingway"
description   = "Zooming User Agent"
license       = "Unlicense"
srcDir        = "src"
bin           = @["zua"]


# Dependencies

requires "nim >= 1.6.0", "syndicate", "nimsvg", "pixie", "sdl2", "https://git.sr.ht/~ehmry/svui"
