# Zooming User Agent

## Goals
- Zoomable
- Network transparent
- Reimplementable
- Display device agnostic (LCD, E-ink, ocular-bypass)

## Further reading
 * [Eaglemode](http://eaglemode.sourceforge.net/)
 * [Pad++](./doc/spe-98-padimplementation.pdf)
 * [A2](https://en.wikipedia.org/wiki/A2_(operating_system))
 * [Sigma Lenses: Focus-Context Transitions Combining Space, Time and Translucence](https://hal.inria.fr/inria-00271301)
 * [Approaches for visualizing large graphs](https://notes.andymatuschak.org/Approaches_for_visualizing_large_graphs)
 * [ZVTM](http://zvtm.sourceforge.net/)
 * [Archy](https://en.wikipedia.org/wiki/Archy_(software))
 * [ZOOMABLE USER INTERFACES IN SCALABLE VECTOR GRAPHICS ](./doc/SvgOpen2007.pdf)
 * [NeWS](https://en.wikipedia.org/wiki/NeWS)

### Text encoding
  * [TEI](https://en.wikipedia.org/wiki/Text_Encoding_Initiative)
  * [XHTML Basic](http://www.w3.org/TR/xhtml-basic)
