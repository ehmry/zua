{
  description = "Zooming User Agent";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/release-21.11";

  outputs = { self, nixpkgs, nimble }:
    let
      systems = [ "aarch64-linux" "x86_64-linux" ];
      forAllSystems = nixpkgs.lib.genAttrs systems;
    in {
      overlay = final: prev: {
        zua = with prev;

          nimPackages.buildNimPackage rec {
            pname = "zua";
            version = "unstable";

            nimBinOnly = true;

            src = self;
            nativeBuildInputs = [ pkg-config ];
            buildInputs = [ fontconfig ] ++ (with nimPackages; [
              bumpy
              chroma
              flatty
              nimsimd
              pixie
              sdl2
              typography
              vmath
              zippy
            ]);

            meta = with lib; {
              description = "Simple RSVP speed-reading utility";
              license = licenses.unlicense;
              homepage = "https://git.sr.ht/~ehmry/hottext";
              maintainers = with maintainers; [ ehmry ];
            };
          };
      };

      defaultPackage = forAllSystems (system:
        let pkgs = nixpkgs.legacyPackages.${system}.extend self.overlay;
        in pkgs.zua);
    };
}
