# SPDX-FileCopyrightText: ☭ 2021 Emery Hemingway
# SPDX-License-Identifier: Unlicense

import std/[asyncdispatch, os, parseopt, xmlparser, xmltree]
import preserves, preserves/parse, preserves/xmlhooks
import syndicate,
  syndicate/[actors, capabilities, relay]

import svui
type Svui = svui.Svui[Ref]

proc main() =
  proc quitHook() {.noconv.} = quit()
  setControlCHook(quitHook)

  waitFor runActor("client") do (turn: var Turn):

    let cap =  mint()

    connectUnix(turn, "/run/syndicate/ds", cap) do (turn: var Turn; a: Assertion) -> TurnAction:
      let ds = unembed a

      for kind, arg, _ in getopt():
        if kind == cmdArgument:
          let megs = getFileSize(arg) shr 20
          if megs > 1:
            stderr.writeLine arg, " is ", megs, " MiB, not publishing"
          else:
            var xml = loadXml(arg)
            if xml.tag != "svg":
              stderr.writeLine arg, " not recognized as SVG"
            var pr = toPreserve(Svui(), Ref)
            pr[1] = toPreserve(xml, Ref)
            discard publish(turn, ds, pr)

  stderr.writeLine "done"

main()
